import 'package:enum_to_string/enum_to_string.dart';

import 'Command.dart';

enum BasicCommands {
  command,
  emergency,
  land,
  stop,
  streamoff,
  streamon,
  takeoff,
}

String commandName(BasicCommands command) {
  switch (command) {
    case BasicCommands.takeoff:
      return "Take Off";
    case BasicCommands.stop:
      return "Stop";
    case BasicCommands.emergency:
      return "Emergency Stop";
    case BasicCommands.command:
      return "Command";
    case BasicCommands.streamoff:
      return "Stop Video";
    case BasicCommands.streamon:
      return "Start Video";
    case BasicCommands.land:
      return "Land";
    default:
      return "Command";
  }
}

class BasicCommand extends Command<BasicCommands> {
  final BasicCommands _commandment;

  BasicCommand(this._commandment);

  @override
  BasicCommands get value {
    return _commandment;
  }

  toString() {
    return EnumToString.convertToString(_commandment);
  }

  @override
  String displayName() {
    return commandName(_commandment);
  }

  @override
  int get hashCode {
    return this._commandment.hashCode;
  }

  @override
  bool operator ==(Object other) {
    if (other is BasicCommand) {
      return other._commandment == this._commandment;
    }
    return false;
  }
}
