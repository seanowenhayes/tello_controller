import 'dart:math';

import 'package:enum_to_string/enum_to_string.dart';

import 'Command.dart';

enum Rotation { cw, ccw }

String commandName(Rotation r) {
  switch (r) {
    case Rotation.cw:
      return "Spin Right";
    case Rotation.ccw:
      return "Spin Left";
    default:
      return "Spin unknown!";
  }
}

class SpinCommand extends Command {
  final Rotation _rotation;
  final int _x;

  SpinCommand(this._rotation, this._x);

  @override
  Rotation get value {
    return _rotation;
  }

  String toString() {
    int x = max(this._x, 1);
    x = min(x, 360);
    String rotation = EnumToString.convertToString(this._rotation);
    return "$rotation $x";
  }

  @override
  String displayName() {
    String rotation = commandName(_rotation);
    return "$rotation $_x degrees";
  }

  @override
  bool operator ==(Object other) {
    if (other is SpinCommand) {
      return other._rotation == this._rotation && other._x == this._x;
    }
    return false;
  }

  @override
  int get hashCode {
    return this._rotation.hashCode * _x;
  }
}
