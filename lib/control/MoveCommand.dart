import 'dart:math';

import 'package:enum_to_string/enum_to_string.dart';

import 'Command.dart';

enum Move {
  up,
  down,
  left,
  right,
  forward,
  back,
}

String commandName(Move move) {
  switch (move) {
    case Move.up:
      return "Up";
    case Move.down:
      return "Down";
    case Move.left:
      return "Left";
    case Move.right:
      return "Right";
    case Move.forward:
      return "Forward";
    case Move.back:
      return "Back";
    default:
      return "Unknown move";
  }
}

class MoveCommand extends Command {
  final int _x;
  final Move _direction;

  MoveCommand(this._x, this._direction);

  @override
  Move get value {
    return _direction;
  }

  String toString() {
    int x = max(this._x, 20);
    x = min(x, 500);
    String direction = EnumToString.convertToString(this._direction);
    return "$direction $x";
  }

  @override
  String displayName() {
    String direction = commandName(_direction);
    return "$direction $_x cm";
  }

  @override
  int get hashCode {
    return this._direction.hashCode * this._x.hashCode;
  }

  Move get move {
    return this._direction;
  }

  @override
  bool operator ==(Object other) {
    if (other is MoveCommand) {
      return other._direction == this._direction &&
          other._direction == this._direction;
    }
    return false;
  }
}
