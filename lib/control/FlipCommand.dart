import 'package:enum_to_string/enum_to_string.dart';

import 'Command.dart';

enum Flip { l, r, f, b }

String commandName(Flip flip) {
  switch (flip) {
    case Flip.b:
      return "Back FLip";
    case Flip.f:
      return "Forward Flip";
    case Flip.l:
      return "Left Flip";
    case Flip.r:
      return "Right Flip";
    default:
      return "Flip what,,,";
  }
}

class FlipCommand extends Command<Flip> {
  final Flip _flip;

  @override
  Flip get value {
    return _flip;
  }

  FlipCommand(this._flip);

  String toString() {
    String flip = EnumToString.convertToString(_flip);
    return "flip $flip";
  }

  @override
  String displayName() {
    return commandName(this._flip);
  }

  @override
  bool operator ==(Object other) {
    if (other is FlipCommand) {
      return other._flip == this._flip;
    }
    return false;
  }

  @override
  int get hashCode {
    return _flip.hashCode;
  }
}
