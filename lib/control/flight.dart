import 'package:tello_controller/connection/tello.dart';

import 'BasicCommand.dart';
import 'Command.dart';
import 'FlipCommand.dart';
import 'MoveCommand.dart';
import 'SpinCommand.dart';

class Flight {
  final Tello _tello;

  Flight(this._tello);

  sendCommand(Command command) {
    _tello.send("$command");
    print("Command sent: $command");
  }

  takeOff() {
    sendCommand(BasicCommand(BasicCommands.command));
    sendCommand(BasicCommand(BasicCommands.takeoff));
  }

  land() {
    sendCommand(BasicCommand(BasicCommands.land));
  }

  stop() {
    sendCommand(BasicCommand(BasicCommands.stop));
  }

  turnOnVideo() {
    sendCommand(BasicCommand(BasicCommands.streamon));
  }

  turnOffVideo() {
    sendCommand(BasicCommand(BasicCommands.streamoff));
  }

  emergencyShutdown() {
    sendCommand(BasicCommand(BasicCommands.emergency));
  }

  /// 20 - 500 cm
  goUp(int distance) {
    sendCommand(MoveCommand(distance, Move.up));
  }

  /// 20 - 500 cm
  goDown(int distance) {
    sendCommand(MoveCommand(distance, Move.down));
  }

  /// 20 - 500 cm
  goLeft(int distance) {
    sendCommand(MoveCommand(distance, Move.left));
  }

  /// 20 - 500 cm
  goRight(int distance) {
    sendCommand(MoveCommand(distance, Move.right));
  }

  /// 20 - 500 cm
  goForwards(int distance) {
    sendCommand(MoveCommand(distance, Move.forward));
  }

  /// 20 - 500 cm
  goBackwards(int distance) {
    sendCommand(MoveCommand(distance, Move.back));
  }

  spinRight(int amount) {
    sendCommand(SpinCommand(Rotation.cw, amount));
  }

  spinLeft(int amount) {
    sendCommand(SpinCommand(Rotation.ccw, amount));
  }

  flipForward() {
    sendCommand(FlipCommand(Flip.f));
  }

  flipBack() {
    sendCommand(FlipCommand(Flip.b));
  }

  flipLeft() {
    sendCommand(FlipCommand(Flip.l));
  }

  flipRight() {
    sendCommand(FlipCommand(Flip.r));
  }
}
