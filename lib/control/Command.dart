import 'package:uuid/uuid.dart';

var uuid = Uuid();

abstract class Command<T> {
  Command() {
    key = uuid.v1();
  }

  String displayName();
  T get value;
  String key;
}
