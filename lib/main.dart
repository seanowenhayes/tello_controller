import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:tello_controller/screens/Home.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Tello flight plan',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
        backgroundColor: Colors.lightGreen,
        buttonColor: Colors.lightGreen,
        primaryColor: Colors.green,
      ),
      routes: {
        'home': (context) => HomePage(title: 'Tello Flight Programmer'),
      },
      initialRoute: 'home',
    );
  }
}
