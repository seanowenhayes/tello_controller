import 'package:flutter/material.dart';
import 'package:tello_controller/forms/FlightModeSelect.dart';

class FlightModeButton extends StatelessWidget {
  final String text;
  final FlightMode flightMode;
  final bool selected;
  final Function onSelect;

  FlightModeButton({this.text, this.flightMode, this.selected, this.onSelect});

  @override
  Widget build(BuildContext context) {
    TextStyle selectedTextStyle = TextStyle(
      color: Colors.white,
    );

    TextStyle unselectedTextStyle = TextStyle(
      color: Colors.black45,
    );
    TextStyle style = selected ? selectedTextStyle : unselectedTextStyle;
    return TextButton(
      child: Text(text, style: style),
      onPressed: () {
        onSelect(flightMode);
      },
      style: TextButton.styleFrom(
        backgroundColor: selected ? Colors.blueAccent : null,
      ),
    );
  }
}
