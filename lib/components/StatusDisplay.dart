import 'package:flutter/material.dart';
import 'package:tello_controller/status/DroneStatus.dart';

class StatusDisplay extends StatefulWidget {
  @override
  _StatusDisplayState createState() => _StatusDisplayState();
}

class _StatusDisplayState extends State<StatusDisplay> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getStatus(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.hasData) {
          return StreamBuilder(
            stream: snapshot.data,
            builder:
                (BuildContext context, AsyncSnapshot<DroneStatus> snapshot) {
              if (snapshot.hasData) {
                DroneStatus status = snapshot.data;
                return Table(children: [
                  TableRow(
                    children: [
                      Text('Speed'),
                      Text('x ${status.vgx}'),
                      Text('y ${status.vgy}'),
                      Text('z ${status.vgz}'),
                    ],
                  ),
                  TableRow(
                    children: [
                      Text('Acceleration'),
                      Text('x ${status.agx}'),
                      Text('y ${status.agy}'),
                      Text('z ${status.agz}'),
                    ],
                  ),
                  TableRow(
                    children: [
                      Text('pitch ${status.pitch}'),
                      Text('roll ${status.roll}'),
                      Text('yaw ${status.yaw}'),
                      Text('tof ${status.tof}'),
                    ],
                  )
                ]);
              }
              return Text("waiting for streaming data...");
            },
          );
        }
        return Text('waiting for future data');
      },
    );
  }
}
