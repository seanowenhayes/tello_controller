import 'package:flutter/material.dart';
import 'package:tello_controller/control/BasicCommand.dart';
import 'package:tello_controller/control/Command.dart';
import 'package:tello_controller/control/FlipCommand.dart';
import 'package:tello_controller/control/MoveCommand.dart';
import 'package:tello_controller/control/SpinCommand.dart';

IconData getCommandIcon(Command command) {
  var value = command.value;
  if (value is BasicCommands) {
    switch (value) {
      case BasicCommands.command:
        return Icons.notifications_active_outlined;
      case BasicCommands.streamon:
        return Icons.video_call;
      case BasicCommands.streamoff:
        return Icons.stop_circle_outlined;
      case BasicCommands.takeoff:
        return Icons.arrow_circle_up_rounded;
      case BasicCommands.stop:
        return Icons.stop;
      case BasicCommands.land:
        return Icons.arrow_circle_down;
      case BasicCommands.emergency:
        return Icons.dangerous;
    }
  }
  if (value is Flip) {
    switch (value) {
      case Flip.r:
        return Icons.flip_camera_android;
      case Flip.l:
        return Icons.flip_camera_android;
      case Flip.f:
        return Icons.forward_5;
      case Flip.b:
        return Icons.settings_backup_restore;
    }
  }
  if (value is Rotation) {
    switch (value) {
      case Rotation.cw:
        return Icons.wb_sunny_outlined;
      case Rotation.ccw:
        return Icons.wb_sunny;
    }
  }
  if (value is Move) {
    switch (value) {
      case Move.back:
        return Icons.arrow_downward;
      case Move.left:
        return Icons.arrow_back;
      case Move.right:
        return Icons.arrow_forward;
      case Move.forward:
        return Icons.arrow_upward;
      case Move.up:
        return Icons.upload_outlined;
      case Move.down:
        return Icons.download_outlined;
    }
  }
  return Icons.device_unknown;
}
