import 'package:flutter/material.dart';
import 'package:tello_controller/components/CommandIcon.dart';
import 'package:tello_controller/control/Command.dart';

class CommandListItem extends StatelessWidget {
  final Command command;
  final Function onRemove;
  final Key key;
  const CommandListItem({this.command, this.onRemove, this.key});

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      key: key,
      child: ListTile(
        title: Text(command.displayName()),
        contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 15),
        subtitle: Text("$command"),
        leading: IconButton(
          icon: Icon(
            getCommandIcon(command),
            color: Colors.black,
          ),
          onPressed: () {
            print("play $command");
          },
        ),
      ),
      onDismissed: (direction) {
        print(direction);
        onRemove(command);
      },
      background: Container(
          color: Colors.black45,
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
          child: Icon(
            Icons.edit,
            color: Colors.white,
          )),
      secondaryBackground: Container(
          color: Colors.red,
          alignment: Alignment.centerRight,
          padding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
          child: Icon(
            Icons.delete,
            color: Colors.white,
          )),
    );
  }
}
