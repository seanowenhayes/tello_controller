import 'dart:math';

import 'package:flutter/material.dart';
import 'package:tello_controller/components/CommandListItem.dart';

import '../control/Command.dart';

class CommandList extends StatefulWidget {
  final List<Command> commands;

  const CommandList({this.commands});

  @override
  _CommandListState createState() => _CommandListState();
}

class _CommandListState extends State<CommandList> {
  @override
  Widget build(BuildContext context) {
    List items = widget.commands.map((command) {
      return CommandListItem(
        key: Key(command.key),
        command: command,
        onRemove: (command) {
          setState(() {
            widget.commands.remove(command);
          });
        },
      );
    }).toList();

    return ReorderableListView(
        children: items,
        onReorder: (startIndex, endIndex) {
          setState(() {
            var command = widget.commands.removeAt(startIndex);
            int end = min(endIndex, widget.commands.length);
            widget.commands.insert(end, command);
          });
        });
  }
}
