import 'dart:io';

import 'package:tello_controller/connection/DroneConnection.dart';

class DroneStatus {
  Map<String, double> _mappedValues = Map();

  double get pitch => _mappedValues['pitch'];
  double get yaw => _mappedValues['yaw'];
  double get roll => _mappedValues['roll'];
  double get vgx => _mappedValues['vgx'];
  double get vgy => _mappedValues['vgy'];
  double get vgz => _mappedValues['vgz'];
  double get templ => _mappedValues['templ'];
  double get temph => _mappedValues['temph'];
  double get tof => _mappedValues['tof'];
  double get h => _mappedValues['h'];
  double get bat => _mappedValues['bat'];
  double get baro => _mappedValues['baro'];
  double get time => _mappedValues['time'];
  double get agx => _mappedValues['agx'];
  double get agy => _mappedValues['agy'];
  double get agz => _mappedValues['agz'];

  DroneStatus(String raw) {
    _mappedValues = Map.fromEntries(
      raw
          .split(';')
          .map((st) => st.split(':'))
          .where((values) => values.length == 2)
          .map((values) => MapEntry(values[0], double.parse(values[1]))),
    );
  }
}

RawDatagramSocket statusSocket;

Future<Stream<DroneStatus>> getStatus() async {
  if (statusSocket == null) {
    statusSocket = await statusConnection();
  }

  return statusSocket.asyncMap((event) {
    Datagram dg = statusSocket.receive();
    if (dg != null) {
      return DroneStatus(String.fromCharCodes(dg.data));
    }
    return DroneStatus('');
  });
}
