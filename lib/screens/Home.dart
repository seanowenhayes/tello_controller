import 'dart:io';

import 'package:flutter/material.dart';
import 'package:tello_controller/components/CommandList.dart';
import 'package:tello_controller/components/StatusDisplay.dart';
import 'package:tello_controller/connection/DroneConnection.dart';
import 'package:tello_controller/control/Command.dart';
import 'package:tello_controller/forms/CommandForm.dart';

import '../actions.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Command> commands = [];

  _doIt() async {
    RawDatagramSocket udpSocket = await droneConnection();
    var commandExecutor = await getCommandExecutor(udpSocket);
    commandExecutor(commands);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(widget.title),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.add,
        ),
        onPressed: () {
          showModalBottomSheet(
              context: context,
              builder: (BuildContext bc) {
                return CommandForm(
                  onSelect: (command) {
                    this.setState(() {
                      commands.add(command);
                    });
                  },
                );
              });
        },
      ),
      body: Column(
        // mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          // Expanded(child: CommandForm()),
          IconButton(
            icon: Icon(
              Icons.play_arrow,
              color: Colors.green,
            ),
            onPressed: () {
              this._doIt();
            },
          ),
          StatusDisplay(),
          Expanded(
            child: CommandList(
              commands: commands,
            ),
          ),
        ],
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
