import 'dart:async';
import 'dart:io';

class Tello {
  InternetAddress droneIpAddress;
  final int dronePort;

  Duration timeout;

  final RawDatagramSocket udpSocket;
  bool sending = false;
  Completer completer;

  Tello(
    this.udpSocket, {
    droneIpAddress = '192.168.10.1',
    this.dronePort = 8889,
    Duration timeout = const Duration(seconds: 5),
  }) {
    this.droneIpAddress = InternetAddress(droneIpAddress);
  }

  Future<bool> send(String message) {
    if (message != null) {
      Completer<bool> completer = Completer();
      udpSocket.send(message.codeUnits, droneIpAddress, dronePort);
      this.completer = completer;
      return completer.future;
    } else {
      return Future.error(Exception("Must have a message to send"));
    }
  }

  void connect() {
    this._connectController();
  }

  _connectController() {
    udpSocket.listen((event) {
      Datagram dg = udpSocket.receive();
      if (dg != null) {
        var str = String.fromCharCodes(dg.data);
        if ("ok" == str) {
          completer.complete(true);
        }
      }
    });
  }
}
