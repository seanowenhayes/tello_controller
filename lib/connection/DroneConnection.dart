import 'dart:io';

Future<RawDatagramSocket> droneConnection({address = '0.0.0.0', port = 9000}) {
  InternetAddress controlAddress = InternetAddress(address);
  return RawDatagramSocket.bind(controlAddress, port);
}

Future<RawDatagramSocket> statusConnection({address = '0.0.0.0', port = 8890}) {
  InternetAddress statusAddress = InternetAddress(address);
  return RawDatagramSocket.bind(statusAddress, port);
}

Future<RawDatagramSocket> videoConnection({address = '0.0.0.0', port: 1111}) {
  InternetAddress videoAddress = InternetAddress(address);
  return RawDatagramSocket.bind(videoAddress, port);
}
