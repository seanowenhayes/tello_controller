import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';

enum FlightMode {
  Basic,
  Move,
  Flip,
  Spin,
}

class FlightModeSelect extends StatefulWidget {
  final FlightMode value;
  final Function onChange;

  FlightModeSelect(this.value, this.onChange);

  @override
  _FlightModeSelectState createState() => _FlightModeSelectState();
}

class _FlightModeSelectState extends State<FlightModeSelect> {
  List items = FlightMode.values
      .map(
        (mode) => DropdownMenuItem(
          value: mode,
          onTap: () {},
          child: Text(
            EnumToString.convertToString(mode),
            style: TextStyle(height: 0),
          ),
        ),
      )
      .toList();

  @override
  Widget build(BuildContext context) {
    return DropdownButton(
      value: this.widget.value,
      onChanged: (value) {
        this.widget.onChange(value);
      },
      items: items,
    );
  }
}
