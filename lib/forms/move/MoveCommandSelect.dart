import 'package:flutter/material.dart';
import 'package:tello_controller/control/MoveCommand.dart';

class MoveCommandSelect extends StatefulWidget {
  final Move move;
  final Function onChange;

  MoveCommandSelect({this.move, this.onChange});

  @override
  _MoveCommandSelectState createState() => _MoveCommandSelectState();
}

class _MoveCommandSelectState extends State<MoveCommandSelect> {
  @override
  Widget build(BuildContext context) {
    List items = Move.values
        .map((move) => DropdownMenuItem(
              child: Text(commandName(move)),
              value: move,
            ))
        .toList();
    return DropdownButton(
      hint: Text("Select Move"),
      value: widget.move,
      onChanged: (value) {
        widget.onChange(value);
      },
      items: items,
    );
  }
}
