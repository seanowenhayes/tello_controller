import 'package:flutter/material.dart';
import 'package:flutter_touch_spin/flutter_touch_spin.dart';
import 'package:tello_controller/control/MoveCommand.dart';
import 'package:tello_controller/forms/move/MoveCommandSelect.dart';

class MoveForm extends StatefulWidget {
  final Function onSelect;

  const MoveForm({this.onSelect});

  @override
  _MoveFormState createState() => _MoveFormState();
}

class _MoveFormState extends State<MoveForm> {
  static const int MIN_DISTANCE = 20;
  static const int MAX_DISTANCE = 500;

  Move direction;
  int howFar = 20;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        MoveCommandSelect(
          move: direction,
          onChange: (move) {
            this.setState(() {
              direction = move;
            });
          },
        ),
        TouchSpin(
          value: howFar,
          min: MIN_DISTANCE,
          max: MAX_DISTANCE,
          step: 5,
          textStyle: TextStyle(fontSize: 24),
          iconSize: 18.0,
          addIcon: Icon(Icons.add_circle_outline),
          subtractIcon: Icon(Icons.remove_circle_outline),
          iconActiveColor: Colors.green,
          iconDisabledColor: Colors.grey,
          iconPadding: EdgeInsets.all(20),
          onChanged: (val) {
            this.setState(() {
              howFar = val;
            });
          },
          enabled: true,
        ),
        RaisedButton(
          color: Colors.blueAccent,
          child: Text(
            "Select",
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          onPressed: () {
            if (direction != null) {
              widget.onSelect(
                MoveCommand(howFar, direction),
              );
            } else {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Text("First select a direction to move."),
              ));
            }
          },
        )
      ],
    );
  }
}
