import 'package:flutter/material.dart';
import 'package:tello_controller/control/FlipCommand.dart';

class FlipCommandSelect extends StatefulWidget {
  final Flip flip;
  final Function onChange;

  FlipCommandSelect({this.flip, this.onChange});

  @override
  _FlipCommandSelectState createState() => _FlipCommandSelectState();
}

class _FlipCommandSelectState extends State<FlipCommandSelect> {
  @override
  Widget build(BuildContext context) {
    List items = Flip.values
        .map((flip) => DropdownMenuItem(
              value: flip,
              child: Text(commandName(flip)),
            ))
        .toList();
    return DropdownButton(
      hint: Text("Select a flip command"),
      value: widget.flip,
      onChanged: (value) {
        widget.onChange(value);
      },
      items: items,
    );
  }
}
