import 'package:flutter/material.dart';
import 'package:tello_controller/control/FlipCommand.dart';
import 'package:tello_controller/forms/flip/FlipCommandSelect.dart';

class FlipForm extends StatefulWidget {
  final Function onSelect;

  const FlipForm({this.onSelect});

  @override
  _FlipFormState createState() => _FlipFormState();
}

class _FlipFormState extends State<FlipForm> {
  Flip flip;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        FlipCommandSelect(
          flip: flip,
          onChange: (flip) {
            this.setState(() {
              this.flip = flip;
            });
          },
        ),
        RaisedButton(
          color: Colors.blueAccent,
          child: Text("SELECT",
              style: TextStyle(
                color: Colors.white,
              )),
          onPressed: () {
            if (flip != null) {
              widget.onSelect(
                FlipCommand(flip),
              );
            } else {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Text("Select a flip first"),
              ));
            }
          },
        ),
      ],
    );
  }
}
