import 'package:flutter/material.dart';
import 'package:tello_controller/components/FlightModeButton.dart';
import 'package:tello_controller/control/Command.dart';
import 'package:tello_controller/forms/basic/BasicForm.dart';
import 'package:tello_controller/forms/move/MoveForm.dart';
import 'package:tello_controller/forms/spin/SpinForm.dart';

import 'FlightModeSelect.dart';
import 'flip/FlipForm.dart';

class CommandForm extends StatefulWidget {
  final Function onSelect;

  const CommandForm({this.onSelect});

  @override
  _CommandFormState createState() => _CommandFormState();
}

Widget getFormItem(FlightMode mode, onSelect) {
  switch (mode) {
    case FlightMode.Basic:
      return BasicForm(
        onSelect: onSelect,
      );
    case FlightMode.Flip:
      return FlipForm(
        onSelect: onSelect,
      );
    case FlightMode.Spin:
      return SpinForm(
        onSelect: onSelect,
      );
    case FlightMode.Move:
      return MoveForm(
        onSelect: onSelect,
      );
    //   return MoveForm(value: value, onChange: onChange);
    default:
      return BasicForm();
  }
}

class _CommandFormState extends State<CommandForm> {
  FlightMode flightMode = FlightMode.Basic;
  Command command;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          ButtonBar(
            alignment: MainAxisAlignment.spaceEvenly,
            children: [
              FlightModeButton(
                  text: "BASIC",
                  flightMode: FlightMode.Basic,
                  selected: flightMode == FlightMode.Basic,
                  onSelect: (selectedFlightMode) {
                    this.setState(() {
                      flightMode = selectedFlightMode;
                    });
                  }),
              FlightModeButton(
                  text: "FLIP",
                  flightMode: FlightMode.Flip,
                  selected: flightMode == FlightMode.Flip,
                  onSelect: (selectedFlightMode) {
                    this.setState(() {
                      flightMode = selectedFlightMode;
                    });
                  }),
              FlightModeButton(
                  text: "SPIN",
                  flightMode: FlightMode.Spin,
                  selected: flightMode == FlightMode.Spin,
                  onSelect: (selectedFlightMode) {
                    this.setState(() {
                      flightMode = selectedFlightMode;
                    });
                  }),
              FlightModeButton(
                  text: "MOVE",
                  flightMode: FlightMode.Move,
                  selected: flightMode == FlightMode.Move,
                  onSelect: (selectedFlightMode) {
                    this.setState(() {
                      flightMode = selectedFlightMode;
                    });
                  }),
            ],
          ),
          getFormItem(flightMode, (Command command) {
            widget.onSelect(command);
            this.setState(() {
              this.command = command;
            });
          }),
        ],
      ),
      height: 150,
    );
  }
}
