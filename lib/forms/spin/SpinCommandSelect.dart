import 'package:flutter/material.dart';
import 'package:tello_controller/control/SpinCommand.dart';

class SpinCommandSelect extends StatefulWidget {
  final Rotation value;
  final Function onChange;

  SpinCommandSelect({this.value, this.onChange});
  @override
  _SpinCommandSelectState createState() => _SpinCommandSelectState();
}

class _SpinCommandSelectState extends State<SpinCommandSelect> {
  @override
  Widget build(BuildContext context) {
    List items = Rotation.values
        .map((spin) => DropdownMenuItem(
              value: spin,
              child: Text(commandName(spin)),
            ))
        .toList();
    return DropdownButton(
      hint: Text("Select Spin"),
      value: this.widget.value,
      onChanged: (value) {
        this.widget.onChange(value);
      },
      items: items,
    );
  }
}
