import 'package:flutter/material.dart';
import 'package:flutter_touch_spin/flutter_touch_spin.dart';
import 'package:tello_controller/control/SpinCommand.dart';
import 'package:tello_controller/forms/spin/SpinCommandSelect.dart';

class SpinForm extends StatefulWidget {
  final Function onSelect;

  const SpinForm({Key key, this.onSelect}) : super(key: key);
  @override
  _SpinFormState createState() => _SpinFormState();
}

class _SpinFormState extends State<SpinForm> {
  static const int MIN_ANGLE = 0;
  static const int MAX_ANGLE = 360;

  Rotation rotation;
  int angle = 0;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        SpinCommandSelect(
            value: rotation,
            onChange: (value) {
              this.setState(() {
                rotation = value;
              });
            }),
        TouchSpin(
          value: angle,
          min: MIN_ANGLE,
          max: MAX_ANGLE,
          step: 5,
          textStyle: TextStyle(fontSize: 24),
          iconSize: 18.0,
          addIcon: Icon(Icons.add_circle_outline),
          subtractIcon: Icon(Icons.remove_circle_outline),
          iconActiveColor: Colors.green,
          iconDisabledColor: Colors.grey,
          iconPadding: EdgeInsets.all(20),
          onChanged: (val) {
            this.setState(() {
              angle = val;
            });
          },
          enabled: true,
        ),
        RaisedButton(
          color: Colors.blueAccent,
          child: Text(
            "Select",
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          onPressed: () {
            if (rotation != null && angle != null) {
              widget.onSelect(
                SpinCommand(rotation, angle),
              );
            } else {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Text("First select a spin"),
              ));
            }
          },
        )
      ],
    );
  }
}
