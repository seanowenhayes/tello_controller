import 'package:flutter/material.dart';
import 'package:tello_controller/control/BasicCommand.dart';

class BasicFlightCommandSelect extends StatefulWidget {
  final BasicCommands value;
  final Function onChange;

  BasicFlightCommandSelect({this.value, this.onChange});

  @override
  _BasicFlightCommandSelectState createState() =>
      _BasicFlightCommandSelectState();
}

class _BasicFlightCommandSelectState extends State<BasicFlightCommandSelect> {
  @override
  Widget build(BuildContext context) {
    List items = BasicCommands.values
        .map((command) => DropdownMenuItem(
              child: Text(commandName(command)),
              value: command,
            ))
        .toList();

    return DropdownButton(
      hint: Text("Select a basic command"),
      value: this.widget.value,
      onChanged: (value) {
        this.widget.onChange(value);
      },
      items: items,
    );
  }
}
