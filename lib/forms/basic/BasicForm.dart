import 'package:flutter/material.dart';
import 'package:tello_controller/control/BasicCommand.dart';
import 'package:tello_controller/forms/basic/BasicFlightCommandSelect.dart';

class BasicForm extends StatefulWidget {
  final Function onSelect;

  const BasicForm({this.onSelect});

  @override
  _BasicFormState createState() => _BasicFormState();
}

class _BasicFormState extends State<BasicForm> {
  BasicCommands value;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        BasicFlightCommandSelect(
          onChange: (value) {
            this.setState(() {
              this.value = value;
            });
          },
          value: value,
        ),
        RaisedButton(
          color: Colors.blueAccent,
          child: Text(
            "SELECT",
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          onPressed: () {
            if (value != null) {
              widget.onSelect(
                BasicCommand(value),
              );
            } else {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Text("First select a command."),
              ));
            }
          },
        )
      ],
    );
  }
}
