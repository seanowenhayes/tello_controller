import 'dart:io';

import 'package:tello_controller/control/BasicCommand.dart';

import 'connection/tello.dart';
import 'control/Command.dart';

Future<void> putDroneIntoSdkMode(Tello tello) async {
  return tello.send(BasicCommand(BasicCommands.command).toString());
}

Future<Function> getCommandExecutor(RawDatagramSocket udpSocket) async {
  Tello tello = Tello(udpSocket);
  await tello.connect();
  print("Connected to tello...");
  await putDroneIntoSdkMode(tello);
  print("Set to sdk mode...");
  return (List<Command> commands) async {
    for (var command in commands) {
      await tello.send(command.toString());
      print("Sent the command to tello: $command");
    }
    print("DONE!");
  };
}
