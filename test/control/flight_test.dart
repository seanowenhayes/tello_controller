import 'package:mockito/mockito.dart';
import 'package:tello_controller/connection/tello.dart';
import 'package:tello_controller/control/flight.dart';
import "package:test/test.dart";

class MockTello extends Mock implements Tello {}

void main() {
  Tello fakeTello;
  Flight flight;

  setUp(() {
    fakeTello = MockTello();
    flight = Flight(fakeTello);
  });

  group('fly', () {
    test('takeoff', () {
      flight.takeOff();
      verify(fakeTello.send("takeoff"));
    });
    test('land', () {
      flight.land();
      verify(fakeTello.send('land'));
    });
    test("stop", () {
      flight.stop();
      verify(fakeTello.send("stop"));
    });
    test("Emergency shutdown", () {
      flight.emergencyShutdown();
      verify(fakeTello.send("emergency"));
    });
    test("back flip", () {
      flight.flipBack();
      verify(fakeTello.send('flip b'));
    });
    test("forward flip", () {
      flight.flipForward();
      verify(fakeTello.send("flip f"));
    });
    test("flip left", () {
      flight.flipLeft();
      verify(fakeTello.send("flip l"));
    });
    test("flip right", () {
      flight.flipRight();
      verify(fakeTello.send('flip r'));
    });
    test("fly backwards", () {
      flight.goBackwards(150);
      verify(fakeTello.send("back 150"));
    });
    test("fly forwards", () {
      flight.goForwards(275);
      verify(fakeTello.send("forward 275"));
    });
    test("fly left", () {
      flight.goLeft(96);
      verify(fakeTello.send("left 96"));
    });
    test("fly right", () {
      flight.goRight(87);
      verify(fakeTello.send("right 87"));
    });
    test("fly down", () {
      flight.goDown(27);
      verify(fakeTello.send("down 27"));
    });
    test("fly up", () {
      flight.goUp(99);
      verify(fakeTello.send("up 99"));
    });
    test("spin left", () {
      flight.spinLeft(90);
      verify(fakeTello.send('ccw 90'));
    });
    test("spin right", () {
      flight.spinRight(180);
      verify(fakeTello.send('cw 180'));
    });
  });
}
