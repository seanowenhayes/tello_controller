import 'package:tello_controller/control/BasicCommand.dart';
import 'package:tello_controller/control/Command.dart';
import 'package:tello_controller/control/FlipCommand.dart';
import 'package:tello_controller/control/MoveCommand.dart';
import 'package:tello_controller/control/SpinCommand.dart';
import "package:test/test.dart";

void main() {
  group("move command", () {
    group("up", () {
      test("100", () {
        Command moveup = MoveCommand(100, Move.up);
        expect(moveup.toString(), equals("up 100"));
      });
      test("600", () {
        Command moveup = MoveCommand(600, Move.up);
        expect(moveup.toString(), equals("up 500"));
      });
      test("-100", () {
        Command moveup = MoveCommand(-100, Move.up);
        expect(moveup.toString(), equals("up 20"));
      });
    });
    group("down", () {
      test("100", () {
        Command move = MoveCommand(100, Move.down);
        expect(move.toString(), equals("down 100"));
      });
      test("600", () {
        Command move = MoveCommand(600, Move.down);
        expect(move.toString(), equals("down 500"));
      });
      test("-100", () {
        Command move = MoveCommand(-100, Move.down);
        expect(move.toString(), equals("down 20"));
      });
    });
    group("left", () {
      test("100", () {
        Command move = MoveCommand(100, Move.left);
        expect(move.toString(), equals("left 100"));
      });
      test("600", () {
        Command move = MoveCommand(600, Move.left);
        expect(move.toString(), equals("left 500"));
      });
      test("-100", () {
        Command move = MoveCommand(-100, Move.left);
        expect(move.toString(), equals("left 20"));
      });
    });
    group("right", () {
      test("100", () {
        Command move = MoveCommand(100, Move.right);
        expect(move.toString(), equals("right 100"));
      });
      test("600", () {
        Command move = MoveCommand(600, Move.right);
        expect(move.toString(), equals("right 500"));
      });
      test("-100", () {
        Command move = MoveCommand(-100, Move.right);
        expect(move.toString(), equals("right 20"));
      });
    });
    group("forward", () {
      test("100", () {
        Command move = MoveCommand(100, Move.forward);
        expect(move.toString(), equals("forward 100"));
      });
      test("600", () {
        Command move = MoveCommand(600, Move.forward);
        expect(move.toString(), equals("forward 500"));
      });
      test("-100", () {
        Command move = MoveCommand(-100, Move.forward);
        expect(move.toString(), equals("forward 20"));
      });
    });
    group("back", () {
      test("100", () {
        Command move = MoveCommand(100, Move.back);
        expect(move.toString(), equals("back 100"));
      });
      test("600", () {
        Command move = MoveCommand(600, Move.back);
        expect(move.toString(), equals("back 500"));
      });
      test("-100", () {
        Command move = MoveCommand(-100, Move.back);
        expect(move.toString(), equals("back 20"));
      });
    });
    group("move basic", () {
      test('takeoff', () {
        Command command = BasicCommand(BasicCommands.takeoff);
        expect(command.toString(), equals("takeoff"));
      });
      test('land', () {
        Command command = BasicCommand(BasicCommands.land);
        expect(command.toString(), equals("land"));
      });
      test('emergency', () {
        Command command = BasicCommand(BasicCommands.emergency);
        expect(command.toString(), equals("emergency"));
      });
      test('stop', () {
        Command command = BasicCommand(BasicCommands.stop);
        expect(command.toString(), equals("stop"));
      });
      test('streamoff', () {
        Command command = BasicCommand(BasicCommands.streamoff);
        expect(command.toString(), equals("streamoff"));
      });
      test('streamon', () {
        Command command = BasicCommand(BasicCommands.streamon);
        expect(command.toString(), equals("streamon"));
      });
    });
    group("spin", () {
      group("right", () {
        test("100", () {
          Command command = SpinCommand(Rotation.cw, 100);
          expect(command.toString(), equals('cw 100'));
        });
        test("-100", () {
          Command command = SpinCommand(Rotation.cw, -100);
          expect(command.toString(), equals('cw 1'));
        });
        test("370", () {
          Command command = SpinCommand(Rotation.cw, 370);
          expect(command.toString(), equals("cw 360"));
        });
      });
      group("left", () {
        test("100", () {
          Command command = SpinCommand(Rotation.ccw, 100);
          expect(command.toString(), equals('ccw 100'));
        });
        test("-100", () {
          Command command = SpinCommand(Rotation.ccw, -100);
          expect(command.toString(), equals('ccw 1'));
        });
        test("370", () {
          Command command = SpinCommand(Rotation.ccw, 370);
          expect(command.toString(), equals("ccw 360"));
        });
      });
    });
    group("flip", () {
      test("forward", () {
        Command command = FlipCommand(Flip.f);
        expect(command.toString(), equals("flip f"));
      });
      test("backwards", () {
        Command command = FlipCommand(Flip.b);
        expect(command.toString(), equals("flip b"));
      });
      test("left", () {
        Command command = FlipCommand(Flip.l);
        expect(command.toString(), equals("flip l"));
      });
      test("right", () {
        Command command = FlipCommand(Flip.r);
        expect(command.toString(), equals("flip r"));
      });
    });
  });
}
