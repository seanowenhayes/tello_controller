import 'package:tello_controller/status/DroneStatus.dart';
import 'package:test/test.dart';

void main() {
  String raw;

  setUp(() {
    raw =
        "pitch:1.1;roll:2.2;yaw:3.3;vgx:4.4;vgy:5.5;vgz:6.6;templ:7.7;temph:8.8;tof:9.9;h:11.11;bat:12.12;baro:13.13;time:1.14;agx:1.15;agy:1.16;agz:1.17;\r\n";
  });

  group('from empty string', () {
    test("It doesn't blow up", () {
      DroneStatus('');
    });
  });

  group('From full string', () {
    DroneStatus status;

    setUp(() {
      status = DroneStatus(raw);
    });

    test('pitch', () {
      expect(status.pitch, equals(1.1));
    });

    test('roll', () {
      expect(status.roll, equals(2.2));
    });

    test('yaw', () {
      expect(status.yaw, equals(3.3));
    });

    test('vgx', () {
      expect(status.vgx, equals(4.4));
    });

    test('vgy', () {
      expect(status.vgy, equals(5.5));
    });

    test('vgz', () {
      expect(status.vgz, equals(6.6));
    });

    test('templ', () {
      expect(status.templ, equals(7.7));
    });

    test('temph', () {
      expect(status.temph, equals(8.8));
    });

    test('tof', () {
      expect(status.tof, equals(9.9));
    });
    test('h', () {
      expect(status.h, equals(11.11));
    });
    test('bat', () {
      expect(status.bat, equals(12.12));
    });
    test('baro', () {
      expect(status.baro, equals(13.13));
    });
    test('time', () {
      expect(status.time, equals(1.14));
    });
    test('agx', () {
      expect(status.agx, equals(1.15));
    });
    test('agy', () {
      expect(status.agy, equals(1.16));
    });
    test('agz', () {
      expect(status.agz, equals(1.17));
    });
  });
}
