import 'dart:io';

import 'package:mockito/mockito.dart';
import 'package:tello_controller/connection/tello.dart';
import 'package:test/test.dart';

class MockSock extends Mock implements RawDatagramSocket {}

void main() {
  Tello tello;
  RawDatagramSocket udpSocket;

  setUp(() {
    udpSocket = MockSock();
    tello = Tello(udpSocket);
  });

  group('#connect', () {
    test('calls listen', () {
      tello.connect();
      // verify(udpSocket.listen());
    });
  });
}
